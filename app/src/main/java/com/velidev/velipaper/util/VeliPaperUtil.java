package com.velidev.velipaper.util;


import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.provider.MediaStore;

import com.velidev.velipaper.service.VeliPaperService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nikola on 8.12.2014..
 */
public class VeliPaperUtil {

    public static int generatePastelColor() {
        Random random = new Random();
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        red = (red + 256) / 2;
        green = (green + 256) / 2;
        blue = (blue + 256) / 2;
        int color = Color.rgb(red, green, blue);
        return color;
    }

    public static int generateDarkPastelColor() {
        Random random = new Random();
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        red = (red) / 2;
        green = (green) / 2;
        blue = (blue) / 2;
        int color = Color.rgb(red, green, blue);
        return color;
    }

    public static int generateColor() {
        Random random = new Random();
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        return Color.rgb(red, green, blue);
    }

    public static void centerText(String text, int x, int y, Paint paint, Canvas canvas) {
        Rect textBounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, x - (textBounds.right + textBounds.left) / 2, y + Math.abs(textBounds.top) / 2, paint);
    }

    public static List<File> getSDCardImagesPath(Context context, boolean fromCache) {
        if (fromCache) {
            List<String> paths = Prefs.getPrefs().getCachedSDCardImagesPaths();
            if (paths != null && paths.size() > 0)
                return pathsToFiles(paths);
        }
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor externalImageCursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, proj, null, null, null);
        Cursor internalImageCursor = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, proj, null, null, null);
        List<String> paths = new ArrayList<>();
        String[] externalImageCursorColumnNames = externalImageCursor.getColumnNames();
        while (externalImageCursor.moveToNext()) {
            for (String columnName : externalImageCursorColumnNames) {
                String columnValue = externalImageCursor.getString(externalImageCursor.getColumnIndex(columnName));
                paths.add(columnValue);
            }
        }

        String[] internalImageCursorColumnNames = internalImageCursor.getColumnNames();
        while (internalImageCursor.moveToNext()) {
            for (String columnName : internalImageCursorColumnNames) {
                String columnValue = internalImageCursor.getString(internalImageCursor.getColumnIndex(columnName));
                paths.add(columnValue);
            }
        }
        Prefs.getPrefs().setSDCardPathsCache(paths);
        return pathsToFiles(paths);
    }

    private static List<File> pathsToFiles(List<String> paths) {
        List<File> files = new ArrayList<>();
        for (String path : paths) {
            files.add(new File(path));
        }
        return files;
    }

    public static int randInt(int min, int max) {
        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        return rand.nextInt((max - min) + 1) + min;
    }

    public static void startWallpaperChooser(Activity context) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT > 15) {
            intent.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(context, VeliPaperService.class));
        } else {
            intent.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
        }
        context.startActivity(intent);
    }

    /**
     * Darken android color by ammount provided.
     *
     * @param color   - Color in Android format
     * @param ammount - How much to darken (must be > 0 && < 1
     * @return
     */
    public static int darkenColor(int color, float ammount) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= ammount; // value component
        return Color.HSVToColor(hsv);
    }

}
