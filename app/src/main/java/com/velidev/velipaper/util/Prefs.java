package com.velidev.velipaper.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.velidev.velipaper.VeliApplication;

import java.util.List;

/**
 * Created by Nikola on 2.1.2015..
 */
public class Prefs {

    /*
    Koji tipovi preferencea ce da se citaju ?

    1. lista rss izvora
    2. analog / saturn
    3. slike, rss ili boja
        3.a. koje boja ? (posle mozda i vise boja)
        3.b. koje slike ?
    //TODO
    4. prikazati tekst
    5. sta da se desi na klik na pozadinu
    6. duzina trajanja izmedju slika
    7. blur
     */

    public static final int CLOCK_MODE_SATURN = 0;
    public static final int CLOCK_MODE_ANALOG = 1;
    public static final int BACKGROUND_MODE_IMAGES = 0;
    public static final int DEFAULT_BACKGROUND_MODE = BACKGROUND_MODE_IMAGES;
    public static final int BACKGROUND_MODE_COLOR = 1;
    public static final int BACKGROUND_MODE_RSS = 2;
    /* DEFAULTS */
    public static final int DEFAULT_DURATION_TRANSITION_TIME = 4;
    public static final int DEFAULT_BLUR = 14;
    public static final int MIN_TRANSITION_TIME = 1;
    public static final int MAX_TRANSITION_TIME = 30;
    public static final int MIN_BLUR = 0;
    public static final int MAX_BLUR = 24;
    private static final String PREFS_PREFIX = "velidev.paper.prefs.";
    private static final String PREFS_CLOCK_MODE = PREFS_PREFIX + "clock_mode";
    private static final String PREFS_BACKGROUND_MODE = PREFS_PREFIX + "background_mode";
    private static final String PREFS_RSS_FEEDS = PREFS_PREFIX + "rss_feeds";
    private static final String PREFS_COLORS = PREFS_PREFIX + "colors";
    private static final String PREFS_IMAGES = PREFS_PREFIX + "images";
    private static final String PREFS_SDCARD_IMAGES_CACHE = PREFS_PREFIX + "paths";
    private static final String PREFS_TRANSITION_TIME = PREFS_PREFIX + "duration";
    private static final String PREFS_BLUR = PREFS_PREFIX + "blur";

    private static int sColor = -1, sClockMode = -1, sBackgroundMode = -1, sTransitionTime = -1, sBlur = -1;
    private static List<String> sImages = null;

    private static Prefs sInstance;
    private final SharedPreferences mPreferences;
    private final Gson mGson;


    private Prefs(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mGson = new Gson();
    }

    public static Prefs getPrefs() {
        if (sInstance == null)
            sInstance = new Prefs(VeliApplication.sContext);
        return sInstance;
    }

    public int getClockMode() {
        sClockMode = mPreferences.getInt(PREFS_CLOCK_MODE, CLOCK_MODE_SATURN);
        return sClockMode;
    }

    public boolean setClockMode(int clockMode) {
        if (clockMode == sClockMode)
            return false;
        mPreferences.edit().putInt(PREFS_CLOCK_MODE, clockMode).apply();
        sClockMode = clockMode;
        return true;
    }

    /**
     * Returns transition duration or DEFAULT_DURATION_TRANSITION_TIME if user didnt set this (in minutes).
     *
     * @return time between transitions
     */
    public int getTransitionTime() {
        sTransitionTime = mPreferences.getInt(PREFS_TRANSITION_TIME, DEFAULT_DURATION_TRANSITION_TIME);
        return sTransitionTime;
    }

    public boolean setTransitionTime(int transitionTime) {
        if (transitionTime == sTransitionTime)
            return false;
        mPreferences.edit().putInt(PREFS_TRANSITION_TIME, transitionTime).apply();
        sTransitionTime = transitionTime;
        return true;
    }

    public int getBlur() {
        sBlur = mPreferences.getInt(PREFS_BLUR, DEFAULT_BLUR);
        return sBlur;
    }

    public boolean setBlur(int blur) {
        if (blur == sBlur)
            return false;
        mPreferences.edit().putInt(PREFS_BLUR, blur).apply();
        sBlur = blur;
        return true;
    }

    public int getBackgroundMode() {
        sBackgroundMode = mPreferences.getInt(PREFS_BACKGROUND_MODE, DEFAULT_BACKGROUND_MODE);
        return sBackgroundMode;
    }

    public boolean setBackgroundMode(int backgroundMode) {
        if (sBackgroundMode == backgroundMode)
            return false;

        mPreferences.edit().putInt(PREFS_BACKGROUND_MODE, backgroundMode).apply();
        sBackgroundMode = backgroundMode;
        return true;
    }

    public List<String> getFeeds() {
        Feeds feeds = getObject(PREFS_RSS_FEEDS, Feeds.class);
        return feeds.getFeeds();
    }

    private <T> T getObject(String key, Class<T> a) {
        String gson = mPreferences.getString(key, null);
        if (gson == null) {
            return null;
        } else {
            try {
                return mGson.fromJson(gson, a);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object storaged with key " + key + " is instanceof other class");
            }
        }
    }

    public void setFeeds(List<String> feeds) {
        putObject(PREFS_RSS_FEEDS, new Feeds(feeds));
    }

    private void putObject(String key, Object object) {
        if (object == null) {
            throw new IllegalArgumentException("object is null");
        }

        if (TextUtils.isEmpty(key)) {
            throw new IllegalArgumentException("key is empty or null");
        }

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key, mGson.toJson(object));
        editor.apply();
    }

    public int getColor() {
        sColor = mPreferences.getInt(PREFS_COLORS, -1);
        return sColor;
    }

    public boolean setColor(int color) {
        if (color == sColor)
            return false;
        mPreferences.edit().putInt(PREFS_COLORS, color).apply();
        sColor = color;
        return true;
    }

    /**
     * Returns paths for user selected images.
     */
    public List<String> getImages() {
        Images images = getObject(PREFS_IMAGES, Images.class);
        if (images == null)
            images = new Images();
        sImages = images.getImages();
        return sImages;
    }

    /**
     * Sets paths after user has selected images which he wants to show.
     *
     * @param images
     */
    public boolean setImages(List<String> images) {
        if (sImages != null && sImages.containsAll(images) && sImages.size() == images.size())
            return false;
        if (images == null)
            return false;
        sImages = images;
        putObject(PREFS_IMAGES, new Images(images));
        return true;
    }

    public List<String> getCachedSDCardImagesPaths() {
        Paths paths = getObject(PREFS_SDCARD_IMAGES_CACHE, Paths.class);
        if (paths == null)
            paths = new Paths();
        return paths.getPaths();
    }

    public void setSDCardPathsCache(List<String> images) {
        putObject(PREFS_SDCARD_IMAGES_CACHE, new Paths(images));
    }

    private static class Feeds {

        List<String> mFeeds;

        public Feeds(List<String> feeds) {
            mFeeds = feeds;
        }

        public Feeds() {
        }

        public List<String> getFeeds() {
            return mFeeds;
        }

        public void setFeeds(List<String> feeds) {
            mFeeds = feeds;
        }
    }

    private static class Images {

        List<String> mImages;

        public Images(List<String> images) {
            mImages = images;
        }

        public Images() {
        }

        public List<String> getImages() {
            return mImages;
        }

        public void setImages(List<String> images) {
            mImages = images;
        }
    }

    private static class Paths {

        List<String> mPaths;

        public Paths(List<String> paths) {
            mPaths = paths;
        }

        public Paths() {
        }

        public List<String> getPaths() {
            return mPaths;
        }

        public void setPaths(List<String> paths) {
            mPaths = paths;
        }
    }

    private static class Colors {

        List<Integer> mColors;

        public Colors(List<Integer> colors) {
            mColors = colors;
        }

        public Colors() {
        }

        public List<Integer> getColors() {
            return mColors;
        }

        public void setColors(List<Integer> colors) {
            mColors = colors;
        }
    }

}


