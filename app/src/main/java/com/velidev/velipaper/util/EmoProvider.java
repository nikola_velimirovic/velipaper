package com.velidev.velipaper.util;

import com.velidev.velipaper.*;

/**
 * Created by Nikola on 6.1.2015..
 */
public class EmoProvider {

    private static final int[] sEmos = new int[10];

    static {
        sEmos[0] = R.drawable.ic_action_emo_angry;
        sEmos[1] = R.drawable.ic_action_emo_basic;
        sEmos[2] = R.drawable.ic_action_emo_cool;
        sEmos[3] = R.drawable.ic_action_emo_evil;
        sEmos[4] = R.drawable.ic_action_emo_kiss;
        sEmos[5] = R.drawable.ic_action_emo_laugh;
        sEmos[6] = R.drawable.ic_action_emo_shame;
        sEmos[7] = R.drawable.ic_action_emo_tongue;
        sEmos[8] = R.drawable.ic_action_emo_wink;
        sEmos[9] = R.drawable.ic_action_emo_wonder;
    }

    public static int getEmo(int position){
        int filter = position % 9;
        return sEmos[filter];
    }

}
