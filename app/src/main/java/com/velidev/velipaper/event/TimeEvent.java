package com.velidev.velipaper.event;

import java.util.*;

/**
 * Created by Nikola on 30.12.2014..
 */
public class TimeEvent extends VeliEvent {

    public String mDayName;
    public int mMinute, mHour, mSecond, mDay, mMonth, mYear, mDaysInMonth;

    public TimeEvent(int source) {
        super(source);
        update();
    }

    public void update() {
        long millis = System.currentTimeMillis();
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        mMinute = c.get(Calendar.MINUTE);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mSecond = c.get(Calendar.SECOND);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mMonth = c.get(Calendar.MONTH);
        mYear = c.get(Calendar.YEAR);
        mDaysInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        mDayName = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
    }

}
