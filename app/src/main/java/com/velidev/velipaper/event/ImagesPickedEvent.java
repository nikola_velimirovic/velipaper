package com.velidev.velipaper.event;

import java.util.List;

/**
 * Created by Nikola on 2/14/2015.
 */
public class ImagesPickedEvent {

    public final List<String> paths;

    public ImagesPickedEvent(List<String> paths) {
        this.paths = paths;
    }
}
