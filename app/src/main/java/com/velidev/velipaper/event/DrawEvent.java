package com.velidev.velipaper.event;

import android.graphics.*;

/**
 * Created by Nikola on 23.12.2014..
 */
public class DrawEvent extends VeliEvent {
    public Canvas canvas;

    public DrawEvent(int sourceId) {
        super(sourceId);
    }

    public void update(Canvas canvas) {
        this.canvas = canvas;
    }
}
