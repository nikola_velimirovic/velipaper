package com.velidev.velipaper.event;

/**
 * Created by Nikola on 23.12.2014..
 */
public class ResizeEvent extends VeliEvent {

    public int centerX;
    public int centerY;
    public int radius;
    public int width, height;

    public ResizeEvent(int sourceId) {
        super(sourceId);
    }

    public void update(int width, int height){
        this.width = width;
        this.height = height + 200;
        centerX = width / 2;
        centerY = (int) (height / 3.35f);
        radius = (int) (width / 3.54f);

    }

}
