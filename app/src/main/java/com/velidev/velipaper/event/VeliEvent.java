package com.velidev.velipaper.event;

/**
 * Created by Nikola on 10.1.2015..
 */
public class VeliEvent {

    private final int mSourceId;

    public VeliEvent(int sourceId) {
        mSourceId = sourceId;
    }

    public int getSourceId() {
        return mSourceId;
    }

}
