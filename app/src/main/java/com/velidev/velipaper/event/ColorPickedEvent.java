package com.velidev.velipaper.event;

/**
 * Created by Nikola on 2/14/2015.
 */
public class ColorPickedEvent {

    public final int color;

    public ColorPickedEvent(int color) {
        this.color = color;
    }
}
