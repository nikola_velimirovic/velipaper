package com.velidev.velipaper.event;

/**
 * Created by Nikola on 2/15/2015.
 */
public class GenericEvent extends VeliEvent {

    public static short REQUEST_UPDATE = 1;
    public static short UPDATE_DONE = 2;

    private short mMessage;

    public GenericEvent(int sourceId) {
        super(sourceId);
    }

    public short getMessage() {
        return mMessage;
    }

    public void setMessage(short message) {
        mMessage = message;
    }

}
