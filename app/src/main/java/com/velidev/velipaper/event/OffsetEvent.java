package com.velidev.velipaper.event;

/**
 * Created by Nikola on 23.12.2014..
 */
public class OffsetEvent extends VeliEvent {

    public   float xOffset, yOffset, xOffsetStep, yOffsetStep;
    public   int xPixelOffset, yPixelOffset;

    public OffsetEvent(int sourceId) {
        super(sourceId);
    }

    public void update(float XOffset, float YOffset, float XOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
        this.xOffset = XOffset;
        this.yOffset = YOffset;
        this.xOffsetStep = XOffsetStep;
        this.yOffsetStep = yOffsetStep;
        this.xPixelOffset = xPixelOffset;
        this.yPixelOffset = yPixelOffset;
    }


}
