package com.velidev.velipaper.event;

import android.graphics.Canvas;
import android.support.v7.graphics.Palette;
import android.util.SparseArray;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 10.1.2015..
 */
public class EventDispatcher {

    public static short DRAW_EVENT = 0;
    public static short OFFSET_EVENT = 1;
    public static short RESIZE_EVENT = 2;
    public static short PALLETE_EVENT = 3;
    public static short TIME_EVENT = 4;
    public static short GENERIC_EVENT = 6;

    private static SparseArray<EventDispatcher> sInstances = new SparseArray<>();
    private final int mSourceId;
    private SparseArray<VeliEvent> mCache = new SparseArray<>();

    private EventDispatcher(int sourceId) {
        mSourceId = sourceId;
    }

    public static EventDispatcher get(int sourceId) {
        EventDispatcher dispatcher = sInstances.get(sourceId);
        if (dispatcher == null) {
            dispatcher = new EventDispatcher(sourceId);
            sInstances.put(sourceId, dispatcher);
        }
        return dispatcher;
    }

    public void sendReziseEvent(int width, int height) {
        VeliEvent event = mCache.get(RESIZE_EVENT);
        if (event == null) {
            event = new ResizeEvent(mSourceId);
            mCache.put(RESIZE_EVENT, event);
        }
        ((ResizeEvent) event).update(width, height);
        EventBus.getDefault().post(event);
    }

    public void sendDrawEvent(Canvas canvas) {
        VeliEvent event = mCache.get(DRAW_EVENT);
        if (event == null) {
            event = new DrawEvent(mSourceId);
            mCache.put(DRAW_EVENT, event);
        }
        ((DrawEvent) event).update(canvas);
        EventBus.getDefault().post(event);
    }

    public void sendOffsetEvent() {
        //TODO sredi ovo sranje
    }

    public void sendPaletteEvent(Palette palette) {
        VeliEvent event = mCache.get(PALLETE_EVENT);
        if (event == null) {
            event = new PaletteEvent(mSourceId);
            mCache.put(PALLETE_EVENT, event);
        }
        ((PaletteEvent) event).update(palette);
        EventBus.getDefault().post(event);
    }

    public void sendTimeEvent() {
        VeliEvent event = mCache.get(TIME_EVENT);
        if (event == null) {
            event = new TimeEvent(mSourceId);
            mCache.put(TIME_EVENT, event);
        }
        ((TimeEvent) event).update();
        EventBus.getDefault().post(event);
    }

    public void sendGenericEvent(short message) {
        VeliEvent event = mCache.get(GENERIC_EVENT);
        if (event == null) {
            event = new GenericEvent(mSourceId);
            mCache.put(GENERIC_EVENT, event);
        }
        ((GenericEvent) event).setMessage(message);
        EventBus.getDefault().post(event);
    }


}
