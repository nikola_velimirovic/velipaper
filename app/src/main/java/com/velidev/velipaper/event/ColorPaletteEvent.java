package com.velidev.velipaper.event;

/**
 * Created by Nikola on 2/14/2015.
 */
public class ColorPaletteEvent {

    public final int color;
    public final int darkColor;

    public ColorPaletteEvent(int color, int darkColor) {
        this.color = color;
        this.darkColor = darkColor;
    }
}
