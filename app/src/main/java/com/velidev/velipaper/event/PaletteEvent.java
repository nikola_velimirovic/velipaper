package com.velidev.velipaper.event;

import android.support.v7.graphics.*;

/**
 * Created by Nikola on 23.12.2014..
 */
public class PaletteEvent extends VeliEvent {

    public Palette palette;

    public PaletteEvent(int sourceId) {
        super(sourceId);
    }

    public void update(Palette palette) {
        this.palette = palette;
    }


}
