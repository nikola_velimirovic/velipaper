package com.velidev.velipaper.event;

/**
 * Created by Nikola on 31.1.2015..
 */
public class UIEvent {

    public static enum UIEventType {
        SHOW_OPTION, RADIO_CHECKED, IMAGE_PICKER_CLOSED
    }

    public int groupId = -1;
    public final int id;
    public final UIEventType type;

    public UIEvent(int id, UIEventType type) {
        this.id = id;
        this.type = type;
    }

    public UIEvent(int id, UIEventType type, int groupId) {
        this.id = id;
        this.type = type;
        this.groupId = groupId;
    }
}
