package com.velidev.velipaper.drawable;

import android.animation.ValueAnimator;
import android.graphics.Canvas;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.event.EventDispatcher;
import com.velidev.velipaper.event.GenericEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.service.PaintProvider;

/**
 * Created by Nikola on 2/15/2015.
 */
public class TransitionDrawable extends VeliDrawable implements ValueAnimator.AnimatorUpdateListener {

    //TODO prebaciti valueanimator u drugi thread
    private ValueAnimator mIncreaseAnimator;
    private ValueAnimator mDecreaseAnimator;
    private int mTransitionMax;
    private int mTransitionRadius = 0;
    private int mCX;
    private int mCY;
    private boolean mDecreasing = false;

    public TransitionDrawable(int sourceId) {
        super(sourceId);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        super.onResize(event);
        mCX = event.centerX;
        mCY = event.centerY;
        mTransitionMax = Math.max(event.width, event.height);
        mIncreaseAnimator = ValueAnimator.ofInt(0, mTransitionMax);
        mDecreaseAnimator = ValueAnimator.ofInt(mTransitionMax, 0);
        mIncreaseAnimator.setDuration(Config.ANIMATION_TIME);
        mDecreaseAnimator.setDuration(Config.ANIMATION_TIME);
        mIncreaseAnimator.addUpdateListener(this);
        mDecreaseAnimator.addUpdateListener(this);
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawCircle(mCX, mCY, mTransitionRadius, PaintProvider.getWhiteShadowPaint());
    }

    public void startTransition() {
        mIncreaseAnimator.start();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        mTransitionRadius = ((Integer) (valueAnimator.getAnimatedValue()));
        if (!mDecreasing && mTransitionRadius >= mTransitionMax) {
            mDecreasing = true;
            mIncreaseAnimator.cancel();
            mDecreaseAnimator.start();
            //zavrsili smo sa povecavanjem
            //saljemo event za osvezavanje
            EventDispatcher.get(mSourceId).sendGenericEvent(GenericEvent.UPDATE_DONE);
        } else if (mTransitionRadius <= 0) {
            mDecreasing = false;
            mDecreaseAnimator.cancel();
        }
    }
}
