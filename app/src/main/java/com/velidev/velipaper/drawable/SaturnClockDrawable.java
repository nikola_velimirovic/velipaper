package com.velidev.velipaper.drawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.support.v7.graphics.Palette;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ColorPaletteEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.event.TimeEvent;
import com.velidev.velipaper.service.PaintProvider;

import javax.inject.Inject;

/**
 * Created by Nikola on 30.12.2014..
 */
public class SaturnClockDrawable extends VeliDrawable {

    private final Paint mPaint;
    @Inject
    Config.ColorProvider mColorProvider;
    private int mCenterY;
    private int mCenterX;
    private int mRadius;
    private String mHourString;
    private String mMinuteString;
    private double mHourAngle;
    private double mMinuteAngle;
    private int mCenterCircleRadius;

    public SaturnClockDrawable(int source) {
        super(source);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(mColorProvider.get(R.color.greenish_ascent_color));
        mPaint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        mCenterX = event.centerX;
        mCenterY = event.centerY;
        mRadius = event.radius;
        float[] positions = {0, .5f, 1f};
        int startColor = mColorProvider.get(android.R.color.holo_blue_light);
        int endColor = mColorProvider.get(R.color.purple_ascent_color);
        SweepGradient sweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{startColor, endColor, startColor}, positions);
        mPaint.setColor(Color.CYAN);
        mCenterCircleRadius = mRadius / 21;
        mPaint.setShader(sweepGradient);
    }

    @Override
    public void onPaletteChanged(Palette palette) {
        //TODO sredi da se ignorisu pallete koje su iste kao i pre
        int startColor = palette.getDarkVibrantColor(mColorProvider.get(android.R.color.holo_blue_light));
        int endColor = palette.getLightVibrantColor(mColorProvider.get(R.color.purple_ascent_color));
        float[] positions = {0, .5f, 1f};
        SweepGradient sweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{startColor, endColor, startColor}, positions);
        mPaint.setColor(Color.CYAN);
        mPaint.setShader(sweepGradient);
    }

    @Override
    protected void onColorPaletteEvent(ColorPaletteEvent event) {
        //TODO sredi da se ignorisu pallete koje su iste kao i pre
        float[] positions = {0, .5f, 1f};
        SweepGradient sweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{event.color, event.darkColor, event.color}, positions);
        mPaint.setColor(Color.CYAN);
        mPaint.setShader(sweepGradient);
    }

    @Override
    protected void onTimeChanged(TimeEvent event) {
        int hour = event.mHour;
        int minute = event.mMinute;
        int second = event.mSecond;
        mHourString = Integer.toString(hour);
        mMinuteString = Integer.toString(minute);
        mMinuteAngle = 2 * Math.PI * (minute / 60.0) + (3 * Math.PI / 2) + 2 * Math.PI * (second / (60.0 * 60));
        mHourAngle = 2 * Math.PI * (hour / 12.0) + (3 * Math.PI / 2) + 2 * Math.PI * (minute / (60.0 * 12)) + 2 * Math.PI * (second / (60.0 * 60 * 12));
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawCircle(mCenterX, mCenterY, mCenterCircleRadius, PaintProvider.getWhiteShadowPaint());
        drawHours(canvas);
        drawMinutes(canvas);
    }

    private void drawHours(Canvas canvas) {
        double v = getMoveThreshold();
        int endX = mCenterX + (int) (Math.cos(mHourAngle) * mRadius * (0.4f - v));
        int endY = mCenterY + (int) (Math.sin(mHourAngle) * mRadius * (0.4f - v));
        canvas.drawCircle(endX, endY, mRadius / 4.52f, PaintProvider.getWhiteShadowPaint());
        mPaint.setTextSize(mRadius / 5.4f);
        Rect textBounds = new Rect();
        mPaint.getTextBounds(mHourString, 0, mHourString.length(), textBounds);
        canvas.drawText(mHourString, endX - (textBounds.right + textBounds.left) / 2, endY + Math.abs(textBounds.top) / 2, mPaint);
    }

    private void drawMinutes(Canvas canvas) {
        int endX = mCenterX + (int) (Math.cos(mMinuteAngle) * mRadius * 0.6f);
        int endY = mCenterY + (int) (Math.sin(mMinuteAngle) * mRadius * 0.6f);
        canvas.drawCircle(endX, endY, mRadius / 6.02f, PaintProvider.getWhiteShadowPaint());
        mPaint.setTextSize(mRadius / 6.3f);
        Rect r = new Rect();
        mPaint.getTextBounds(mMinuteString, 0, mMinuteString.length(), r);
        canvas.drawText(mMinuteString, endX - (r.right + r.left) / 2, endY + Math.abs(r.top) / 2, mPaint);
    }

    private double getMoveThreshold() {
        double had = Math.toDegrees(mHourAngle) % 360;
        double mad = Math.toDegrees(mMinuteAngle) % 360;
        double diff = had > mad ? Math.abs(had - mad) : Math.abs(mad - had);
        if (diff <= 30) {
            return 0.4d - diff / 100;
        }
        return 0;
    }
}
