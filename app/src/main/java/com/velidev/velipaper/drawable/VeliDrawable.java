package com.velidev.velipaper.drawable;

import android.graphics.Canvas;
import android.support.v7.graphics.Palette;

import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.event.ColorPaletteEvent;
import com.velidev.velipaper.event.DrawEvent;
import com.velidev.velipaper.event.EventDispatcher;
import com.velidev.velipaper.event.OffsetEvent;
import com.velidev.velipaper.event.PaletteEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.event.TimeEvent;
import com.velidev.velipaper.log.Logger;

import org.mcsoxford.rss.RSSItem;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 20.12.2014..
 */
public abstract class VeliDrawable {

    protected final int mSourceId;
    @Inject
    @Named("VeliDrawable")
    Logger log;

    public VeliDrawable(int sourceId) {
        VeliApplication.inject(this);
        mSourceId = sourceId;
        EventBus.getDefault().register(this);
    }

    public void onEvent(ResizeEvent event) {
        if (event.getSourceId() == mSourceId)
            onResize(event);
    }

    protected void onResize(ResizeEvent event) {

    }

    public void onEvent(OffsetEvent event) {
        if (event.getSourceId() == mSourceId)
            onOffset(event.xPixelOffset);
    }

    protected void onOffset(int xPixelOffset) {

    }

    public void onEvent(PaletteEvent event) {
        if (event.getSourceId() == mSourceId)
            onPaletteChanged(event.palette);
    }

    public void onPaletteChanged(Palette palette) {
        //STUB
    }

    public void onEvent(ColorPaletteEvent event) {
        onColorPaletteEvent(event);
    }

    protected void onColorPaletteEvent(ColorPaletteEvent event) {

    }

    public void onEvent(TimeEvent event) {
        if (event.getSourceId() == mSourceId) onTimeChanged(event);
    }

    protected void onTimeChanged(TimeEvent event) {
    }

    public void onEvent(RSSItem item) {
        //TODO zasto ovo nije event
        onRSSUpdate(item);
    }

    public void onRSSUpdate(RSSItem item) {

    }

    public void onDrawEvent(DrawEvent drawEvent) {
        if (drawEvent.getSourceId() == mSourceId)
            onDraw(drawEvent.canvas);
    }

    public abstract void onDraw(Canvas canvas);

    protected EventDispatcher getEventDispatcher() {
        return EventDispatcher.get(mSourceId);
    }


}
