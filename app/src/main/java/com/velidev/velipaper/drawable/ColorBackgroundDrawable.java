package com.velidev.velipaper.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.SweepGradient;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ColorPaletteEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.util.Prefs;
import com.velidev.velipaper.util.VeliPaperUtil;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 24.1.2015..
 */
public class ColorBackgroundDrawable extends VeliDrawable {

    private final int mColor;
    private final Paint mPaint;
    @Inject
    Config.ColorProvider mColorProvider;
    private int mHeight;
    private int mWidth;

    public ColorBackgroundDrawable(int sourceId) {
        super(sourceId);
        final int color = Prefs.getPrefs().getColor();
        mColor = color == -1 ? mColorProvider.get(R.color.purple_ascent_color) : color;
        mPaint = new Paint();
        mPaint.setColor(mColor);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        super.onResize(event);
        mWidth = event.width;
        mHeight = event.height;
        int startColor = mColor;
        int endColor = VeliPaperUtil.darkenColor(mColor, 0.65f);
        float[] positions = {0, 0.5f, 1f};
        SweepGradient sweepGradient = new SweepGradient(event.centerX, event.centerY, new int[]{endColor, startColor, endColor}, positions);
        mPaint.setShader(sweepGradient);
        EventBus.getDefault().post(new ColorPaletteEvent(mColor, endColor));
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, mWidth, mHeight, mPaint);
    }
}
