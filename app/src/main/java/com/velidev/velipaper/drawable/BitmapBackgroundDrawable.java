package com.velidev.velipaper.drawable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.graphics.Palette;

import com.squareup.picasso.Picasso;
import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.event.EventDispatcher;
import com.velidev.velipaper.event.GenericEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.event.TimeEvent;
import com.velidev.velipaper.util.Blur;
import com.velidev.velipaper.util.Prefs;

import java.io.File;
import java.util.List;

/**
 * Created by Nikola on 24.1.2015..
 */
public class BitmapBackgroundDrawable extends VeliDrawable {

    public static final float MAX_WIDTH = 400f;
    public static final float MAX_HEIGHT = 600f;
    private final List<String> mImagesPaths;
    private final Paint mPaint;
    private int mTransitionTime;
    private long mLastLoadTimestamp = -1;
    private int mPosition = 0;
    private Bitmap mBitmap;
    private boolean mBitmapLoading = false;
    private float mBitmapWidth;
    private float mBitmapHeight;
    private Rect mSrc;
    private Rect mDest;
    private int mLastLoadedPosition = -1;
    private boolean mRequestedUpdate = false;

    public BitmapBackgroundDrawable(int sourceId) {
        super(sourceId);
        mImagesPaths = Prefs.getPrefs().getImages();
        mTransitionTime = Prefs.getPrefs().getTransitionTime() * 1000 * 60;
        mPaint = new Paint();
    }

    @Override
    protected void onResize(ResizeEvent event) {
        //max nek bude visina 600 i sirina 400
        float widthRatio = event.width > MAX_WIDTH ? MAX_WIDTH / event.width : event.width / MAX_WIDTH;
        float heightRatio = event.height > MAX_HEIGHT ? MAX_HEIGHT / event.height : event.height / MAX_HEIGHT;
        mBitmapWidth = widthRatio * event.width;
        mBitmapHeight = heightRatio * event.height;

        mSrc = new Rect();
        mSrc.left = 0;
        mSrc.top = 0;
        mSrc.bottom = (int) mBitmapHeight;
        mSrc.right = (int) mBitmapWidth;

        mDest = new Rect();
        mDest.left = 0;
        mDest.top = 0;
        mDest.bottom = event.height;
        mDest.right = event.width;

        log.w("on resize > width: " + mBitmapWidth + " height: " + mBitmapHeight);
    }

    @Override
    protected void onTimeChanged(TimeEvent event) {
        loadNext();
    }

    @Override
    public void onDraw(Canvas canvas) {
        log.d("on draw");
        if (mBitmap != null) {
            log.d("drawing bitmap  > " + mBitmap + " h: " + mBitmap.getHeight() + " w: " + mBitmap.getWidth());
            canvas.drawBitmap(mBitmap, mSrc, mDest, mPaint);
        }
    }

    private void loadNext() {
        if (mImagesPaths != null && mImagesPaths.size() > 0) {
            if (mLastLoadedPosition == mPosition)
                return;

            long time = System.currentTimeMillis();
            log.d("load next bitmaploading " + mBitmapLoading + " time ok ? " + (mLastLoadTimestamp + mTransitionTime) + " < " + time);

            if (!mBitmapLoading && !mRequestedUpdate && mLastLoadTimestamp + mTransitionTime < time) {
                mRequestedUpdate = true;
                EventDispatcher.get(mSourceId).sendGenericEvent(GenericEvent.REQUEST_UPDATE);
            }
        }
    }

    public void onEvent(GenericEvent event) {
        //TODO
        if (mRequestedUpdate && event.getMessage() == GenericEvent.UPDATE_DONE) {
            mBitmapLoading = true;
            String path = mImagesPaths.get(mPosition);
            log.i("loading path: " + path + " position " + mPosition);
            BitmapPreloadAsyncTask task = new BitmapPreloadAsyncTask();
            task.execute(path);
            mLastLoadedPosition = mPosition;
            mPosition = (mPosition + 1) % mImagesPaths.size();
            mRequestedUpdate = false;
        }
    }

    private class BitmapPreloadAsyncTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            String path = params[0];
            try {
                Uri uri = Uri.fromFile(new File(path));
                log.i("loading bitmap in background");
                Bitmap srcBmp = Picasso.with(VeliApplication.sContext).load(uri).resize((int) mBitmapWidth, (int) mBitmapHeight).centerCrop().get();
                int blur = Prefs.getPrefs().getBlur();
                if (blur > 0) {
                    log.i("image loaded bluring to radius of " + blur);
                    Bitmap bluredBitmap = Blur.fastblur(VeliApplication.sContext, srcBmp, blur);
                    Palette palette = Palette.generate(srcBmp);
                    getEventDispatcher().sendPaletteEvent(palette);
                    log.i("pallete dispatched");
                    return bluredBitmap;
                } else {
                    return srcBmp;
                }
            } catch (Exception e) {
                log.e("failed loading bitmap", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            log.i("on post execute " + bitmap);
            if (bitmap != null) {
                mBitmap = bitmap;
            }
            mBitmapLoading = false;
            mLastLoadTimestamp = System.currentTimeMillis();
        }
    }

}
