package com.velidev.velipaper.drawable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.graphics.Palette;

import com.squareup.picasso.Picasso;
import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.service.VeliPaperService;
import com.velidev.velipaper.util.Blur;

import org.mcsoxford.rss.RSSItem;

import javax.inject.Inject;

/**
 * Created by Nikola on 8.12.2014..
 */
public class RssBackgroundDrawable extends VeliDrawable {

    @Inject
    Config.ColorProvider mColorProvider;

    private final Paint mPaint;
    private AsyncTask<Void, Void, Bitmap> mTask = null;
    private Bitmap mBitmap;
    private Palette mPalette;
    private int mWidth;
    private int mHeight;

    public RssBackgroundDrawable(int source) {
        super(source);
        mPaint = new Paint();
        mPaint.setAlpha(60);
    }

    @Override
    public void onDraw(Canvas canvas) {
        int bgdColor = mPalette != null ? mPalette.getDarkVibrantColor(R.color.light_gray) : mColorProvider.get(R.color.light_gray);
        canvas.drawColor(bgdColor);
        if (mBitmap != null) {
            Rect src = new Rect();
            src.left = 0;
            src.right = mBitmap.getWidth();
            src.top = 0;
            src.bottom = mBitmap.getHeight();

            Rect dest = new Rect();
            dest.left = 0;
            dest.right = mWidth;
            dest.top = 0;
            dest.bottom = mHeight;

            canvas.drawBitmap(mBitmap, src, dest, mPaint);
        }
    }

    public void onPaletteChanged(Palette palette) {
        mPalette = palette;
    }

    @Override
    protected void onResize(ResizeEvent event) {
        mWidth = event.width;
        mHeight = event.height;
    }

    @Override
    public void onRSSUpdate(final RSSItem item) {
        if (mTask != null)
            mTask.cancel(true);

        mTask = new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                if (item.getEnclosure() == null)
                    return null;


                Uri url = item.getEnclosure().getUrl();
                try {
                    log.d("loading bitmap in background");
                    Bitmap srcBmp = Picasso.with(VeliPaperService.context).load(url.toString()).get();
                    Bitmap dstBmp = ThumbnailUtils.extractThumbnail(srcBmp, mWidth, mHeight);
                    Bitmap bluredBitmap = Blur.fastblur(VeliPaperService.context, dstBmp, 15);
                    Palette palette = Palette.generate(dstBmp);
                    getEventDispatcher().sendPaletteEvent(palette);
                    return bluredBitmap;
                } catch (Exception e) {
                    log.e("failed loading image", e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null)
                    mBitmap = bitmap;
            }
        };
        mTask.execute((Void[]) null);
    }
}
