package com.velidev.velipaper.drawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ColorPaletteEvent;
import com.velidev.velipaper.event.ResizeEvent;
import com.velidev.velipaper.event.TimeEvent;
import com.velidev.velipaper.service.PaintProvider;

import javax.inject.Inject;

/**
 * Created by Nikola on 30.12.2014..
 */
public class CircularDateDrawable extends VeliDrawable {

    private final Paint mPaint;
    @Inject
    Config.ColorProvider mColorProvider;
    private int mCenterX;
    private int mCenterY;
    private int mRadius;
    private RectF mOval;
    private float mSweepAngle = 0;
    private int mDayEndX;
    private int mDayEndY;
    private float mDayCircleRadius;
    private String mDayString;
    private Rect mDateTextBounds;
    private String mDayName;
    private Rect mDayNameTextBounds;
    private int mWidth;
    private SweepGradient mSweepGradient;

    public CircularDateDrawable(int source) {
        super(source);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        mWidth = event.width;
        mCenterY = event.centerY;
        mCenterX = event.centerX;
        mRadius = event.radius;
        int strokeWidth = mRadius / 15;
        mOval = new RectF();
        mOval.set(mCenterX - mRadius, mCenterY - mRadius, mCenterX + mRadius, mCenterY + mRadius);
        mPaint.setStrokeWidth(strokeWidth);
        mDayCircleRadius = mRadius / 8.3f;
        mPaint.setTextSize(mRadius / 6.8f);
        float[] positions = {0, .5f, 1f};
        int startColor = mColorProvider.get(android.R.color.background_light);
        int endColor = mColorProvider.get(R.color.purple_ascent_color);
        mSweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{startColor, endColor, startColor}, positions);
        mPaint.setColor(Color.CYAN);
        mPaint.setShader(mSweepGradient);
    }

    @Override
    public void onPaletteChanged(Palette palette) {
        int startColor = palette.getDarkVibrantColor(mColorProvider.get(android.R.color.background_light));
        int endColor = palette.getLightVibrantColor(mColorProvider.get(R.color.purple_ascent_color));
        float[] positions = {0, .5f, 1f};
        mSweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{startColor, endColor, startColor}, positions);

    }

    @Override
    protected void onColorPaletteEvent(ColorPaletteEvent event) {
        float[] positions = {0, .5f, 1f};
        mSweepGradient = new SweepGradient(mCenterX, mCenterY, new int[]{event.color, event.darkColor, event.color}, positions);
    }

    @Override
    protected void onTimeChanged(TimeEvent event) {
        mSweepAngle = (360f / event.mDaysInMonth) * event.mDay;
        double dayAngle = Math.toRadians(mSweepAngle) + +(3 * Math.PI / 2);
        mDayEndX = mCenterX + (int) (Math.cos(dayAngle) * mRadius);
        mDayEndY = mCenterY + (int) (Math.sin(dayAngle) * mRadius);
        mDateTextBounds = new Rect();
        mDayString = Integer.toString(event.mDay);
        mPaint.getTextBounds(mDayString, 0, mDayString.length(), mDateTextBounds);
        mDayName = event.mDayName;
        if (!TextUtils.isEmpty(mDayName)) {
            mDayName = mDayName.toUpperCase();
            Paint whiteShadowPaint = PaintProvider.getWhiteShadowPaint();//.setTextSize(mRadius / 6);
            whiteShadowPaint.setTextSize(mRadius / 8.5f);
            whiteShadowPaint.setTypeface(Typeface.DEFAULT_BOLD);
            mDayNameTextBounds = new Rect();
            whiteShadowPaint.getTextBounds(mDayName, 0, mDayName.length(), mDayNameTextBounds);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mColorProvider.get(R.color.transparent_gray_color));
        canvas.drawCircle(mCenterX, mCenterY, mRadius, mPaint);
        mPaint.setColor(Color.CYAN);
        mPaint.setShader(mSweepGradient);
        canvas.drawArc(mOval, -90, mSweepAngle, false, mPaint);
        canvas.drawCircle(mDayEndX, mDayEndY, mDayCircleRadius, PaintProvider.getWhiteShadowPaint());
        mPaint.setColor(mColorProvider.get(R.color.greenish_ascent_color));
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawText(mDayString, mDayEndX - (mDateTextBounds.right + mDateTextBounds.left) / 2, mDayEndY + Math.abs(mDateTextBounds.top) / 2, mPaint);
        mPaint.setShader(null);
        if (!TextUtils.isEmpty(mDayName)) {
            int startRegion = (mWidth - mRadius * 2) / 2;
            int dayNameX = startRegion / 2 - (mDayNameTextBounds.right + mDayNameTextBounds.left) / 2;
            canvas.drawText(mDayName, dayNameX, mCenterY + Math.abs(mDayNameTextBounds.top) / 2, PaintProvider.getWhiteShadowPaint());
        }
    }
}
