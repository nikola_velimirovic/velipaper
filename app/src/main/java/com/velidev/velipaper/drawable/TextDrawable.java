package com.velidev.velipaper.drawable;

import android.graphics.*;
import android.text.*;

import com.velidev.velipaper.event.*;
import com.velidev.velipaper.service.*;

import org.mcsoxford.rss.*;

/**
 * Created by Nikola on 8.12.2014..
 */
public class TextDrawable extends VeliDrawable {

    private int mStartY;
    private int mStartX;
    private float mWidth;
    private StaticLayout mTextLayout;

    public TextDrawable(int source) {
        super(source);
    }

    @Override
    public void onRSSUpdate(RSSItem item) {
        mTextLayout = new StaticLayout(item.getTitle(), PaintProvider.getWhiteShadowTextPaint(), (int) mWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        mStartY = event.centerY + event.radius + event.radius / 6;
        mStartX = event.centerX;
        mWidth = event.width * 0.9f;
        PaintProvider.getWhiteShadowTextPaint().setTextSize(event.radius / 6.5f);
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (mTextLayout != null) {
            canvas.save();
            canvas.translate(mStartX, mStartY);
            mTextLayout.draw(canvas);
            canvas.restore();
        }
    }
}
