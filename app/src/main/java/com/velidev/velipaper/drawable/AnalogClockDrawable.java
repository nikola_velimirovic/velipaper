package com.velidev.velipaper.drawable;

import android.graphics.*;

import com.velidev.velipaper.event.*;
import com.velidev.velipaper.service.*;

/**
 * Created by Nikola on 31.12.2014..
 */
public class AnalogClockDrawable extends VeliDrawable {
    private int mCenterX, mCenterY;
    private double mMinuteAngle;
    private double mHourAngle;
    private double mMinuteOpoAngle;
    private double mHourOpoAngle;
    private int mRadius;

    public AnalogClockDrawable(int sourceId) {
        super(sourceId);
    }

    @Override
    protected void onResize(ResizeEvent event) {
        mCenterX = event.centerX;
        mCenterY = event.centerY;
        mRadius = event.radius;
    }

    @Override
    protected void onTimeChanged(TimeEvent event) {
        int hour = event.mHour;
        int minute = event.mMinute;
        int second = event.mSecond;
        mMinuteAngle = 2 * Math.PI * (minute / 60.0) + (3 * Math.PI / 2) + 2 * Math.PI * (second / (60.0 * 60));
        mMinuteOpoAngle = mMinuteAngle + Math.PI;
        mHourAngle = 2 * Math.PI * (hour / 12.0) + (3 * Math.PI / 2) + 2 * Math.PI * (minute / (60.0 * 12)) + 2 * Math.PI * (second / (60.0 * 60 * 12));
        mHourOpoAngle = mHourAngle + Math.PI;
    }

    @Override
    public void onDraw(Canvas canvas) {
        drawHour(canvas);
        drawMinutes(canvas);
     }

    private void drawMinutes(Canvas canvas) {
        int startX = mCenterX + (int) (Math.cos(mMinuteOpoAngle) * mRadius * 0.12f);
        int startY = mCenterY + (int) (Math.sin(mMinuteOpoAngle) * mRadius * 0.12f);
        int endX = mCenterX + (int) (Math.cos(mMinuteAngle) * mRadius * 0.8f);
        int endY = mCenterY + (int) (Math.sin(mMinuteAngle) * mRadius * 0.8f);
        PaintProvider.getWhiteShadowPaint().setStrokeWidth(mRadius * 0.05f);
        canvas.drawLine(startX, startY, endX, endY, PaintProvider.getWhiteShadowPaint());
    }

    private void drawHour(Canvas canvas) {
        int startX = mCenterX + (int) (Math.cos(mHourOpoAngle) * mRadius * 0.12f);
        int startY = mCenterY + (int) (Math.sin(mHourAngle) * mRadius * 0.12f);
        int endX = mCenterX + (int) (Math.cos(mHourAngle) * mRadius * 0.5f);
        int endY = mCenterY + (int) (Math.sin(mHourAngle) * mRadius * 0.5f);
        PaintProvider.getWhiteShadowPaint().setStrokeWidth(mRadius * 0.06f);
        canvas.drawLine(startX, startY, endX, endY, PaintProvider.getWhiteShadowPaint());
    }
}
