package com.velidev.velipaper;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseArray;

/**
 * Created by Nikola on 8.12.2014..
 */
public class Config {

    public static boolean DEBUG = true;
    public static int ANIMATION_TIME = 400;
    public static int ANIMATION_TIME_SHORT = 200;
    public static int ANIMATION_TIME_SHORTEST = 200;
    public static final int TRANSITION_DURATION = 15000;


    public static final class ColorProvider {

        private final SparseArray<Integer> mColors = new SparseArray<>();
        private final Context mContext;
        private final Resources mRes;

        public ColorProvider(Context context) {
            mContext = context;
            mRes = mContext.getResources();
        }

        public int get(int colorId) {
            if (mColors.indexOfKey(colorId) >= 0)
                return mColors.get(colorId);
            int color = mRes.getColor(colorId);
            mColors.put(colorId, color);
            return color;
        }

    }
}
