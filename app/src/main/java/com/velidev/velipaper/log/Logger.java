package com.velidev.velipaper.log;


import android.util.*;

public class Logger {

    public static final String DEFAULT_LOG_TAG = "vp";
    private static final String LOG_PREFIX = "vp_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 30;
    private String mTag;

    public static String makeLogTag(String str) {
        String logTag = DEFAULT_LOG_TAG;

        if (str == null) {
            return logTag;
        }

        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            logTag = LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        } else {
            logTag = LOG_PREFIX + str;
        }
        return logTag;
    }


    public Logger() {
        mTag = DEFAULT_LOG_TAG;
    }

    private Logger(String tag) {
        if (tag == null)
            tag = DEFAULT_LOG_TAG;
        mTag = tag;
    }


    private Logger(Class tag) {
        if (tag == null) {
            mTag = DEFAULT_LOG_TAG;
        } else {
            mTag = tag.getSimpleName();
        }
    }

    public static Logger get() {
        return new Logger();
    }

    public static Logger get(String tag) {
        return new Logger(tag);
    }

    public static Logger get(Class clas) {
        return new Logger(clas);
    }

    public void w(String message) {
        Log.w(mTag, message);
    }

    public void e(String message) {
        Log.e(mTag, message);
    }

    public void e(Throwable e) {
        Log.e(mTag, "error: ", e);
    }

    public void e(String message, Throwable e) {
        Log.e(mTag, message, e);
    }

    public void v(String message) {
        Log.v(mTag, message);
    }

    public void d(String message) {
        Log.d(mTag, message);
    }

    public void i(String message) {
        Log.i(mTag, message);
    }

}
