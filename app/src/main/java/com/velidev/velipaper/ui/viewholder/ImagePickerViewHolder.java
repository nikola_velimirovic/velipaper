package com.velidev.velipaper.ui.viewholder;

import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import com.velidev.velipaper.*;

/**
 * Created by Nikola on 5.1.2015..
 */
public class ImagePickerViewHolder extends RecyclerView.ViewHolder {

    public ImageView mImage, mEmo;
    public View mSelectedOverlay;

    public ImagePickerViewHolder(View itemView) {
        super(itemView);
        mImage = (ImageView) itemView.findViewById(R.id.image);
        mEmo = (ImageView) itemView.findViewById(R.id.emo);
        mSelectedOverlay = itemView.findViewById(R.id.selected_overlay);
    }

}
