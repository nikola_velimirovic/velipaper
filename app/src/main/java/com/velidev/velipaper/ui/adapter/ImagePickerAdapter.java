package com.velidev.velipaper.ui.adapter;

import android.content.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;
import android.view.animation.*;
import android.widget.*;

import com.squareup.picasso.*;
import com.velidev.velipaper.*;
import com.velidev.velipaper.ui.viewholder.*;
import com.velidev.velipaper.util.*;

import java.io.*;
import java.util.*;

import de.greenrobot.event.*;

/**
 * Created by Nikola on 5.1.2015..
 */
public class ImagePickerAdapter extends RecyclerView.Adapter<ImagePickerViewHolder> {

    public static class ImageAdapterInfo {
        public int selected = 0;
        public int count = 0;
        public List<File> mSelectedFiles = new ArrayList<>();
    }

    private Context mContext;
    private final List<File> mFiles;
    private final LayoutInflater mLayoutInflater;
    private int mSize;
    private int mColumnCount;
    private SparseBooleanArray mSelectedPositions = new SparseBooleanArray();
    private SparseBooleanArray mAnimatedPositions = new SparseBooleanArray();
    private ImageAdapterInfo mInfo;

    public ImagePickerAdapter(Context context, List<File> files, int width) {
        mContext = context;
        mColumnCount = context.getResources().getInteger(R.integer.column_count);
        mLayoutInflater = LayoutInflater.from(mContext);
        mFiles = files;
        mSize = width / mColumnCount;


        notifyDataSetChanged();
    }

    public void reset(Context context, int width) {
        mContext = context;
        mColumnCount = context.getResources().getInteger(R.integer.column_count);
        mSize = width / mColumnCount;
        notifyDataSetChanged();
        sendEvent();
    }

    @Override
    public ImagePickerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mLayoutInflater.inflate(R.layout.view_image_picker, null);
        return new ImagePickerViewHolder(view);
    }

    public void add(File file) {
        int position = mFiles.size();
        mFiles.add(file);
        notifyItemInserted(position);
    }

    @Override
    public void onBindViewHolder(final ImagePickerViewHolder viewHolder, final int i) {
        Picasso.with(mContext).load(mFiles.get(i)).placeholder(R.color.more_transparent_purple).resize(mSize, mSize).centerCrop().into(viewHolder.mImage);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) viewHolder.mImage.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        layoutParams.width = mSize;
        layoutParams.height = mSize;

        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (mContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, mContext.getResources().getDisplayMetrics());
        }
        actionBarHeight += 30;
        //just added padding on top so that we go under list
        if (i < mColumnCount) {
            int pl = viewHolder.itemView.getPaddingLeft();
            int pr = viewHolder.itemView.getPaddingRight();
            int pb = viewHolder.itemView.getPaddingBottom();
            viewHolder.itemView.setPadding(pl, actionBarHeight, pr, pb);
        } else {
            int pl = viewHolder.itemView.getPaddingLeft();
            int pr = viewHolder.itemView.getPaddingRight();
            int pb = viewHolder.itemView.getPaddingBottom();
            viewHolder.itemView.setPadding(pl, 0, pr, pb);
        }
        viewHolder.mImage.setLayoutParams(layoutParams);

        final boolean selected = mSelectedPositions.get(i);
        if (selected) {
            if (mAnimatedPositions.get(i)) {
                mAnimatedPositions.put(i, false);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
                animation.setDuration(200);
                viewHolder.mSelectedOverlay.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        boolean selected = mSelectedPositions.get(viewHolder.getPosition());
                        viewHolder.mSelectedOverlay.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
                        viewHolder.mEmo.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
                        viewHolder.mEmo.setImageResource(EmoProvider.getEmo(i));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            } else {
                viewHolder.mSelectedOverlay.setVisibility(View.VISIBLE);
                viewHolder.mEmo.setVisibility(View.VISIBLE);
                viewHolder.mEmo.setImageResource(EmoProvider.getEmo(i));
            }

        } else {
            if (mAnimatedPositions.get(i)) {
                mAnimatedPositions.put(i, false);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
                animation.setDuration(200);
                viewHolder.mSelectedOverlay.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        boolean selected = mSelectedPositions.get(viewHolder.getPosition());
                        viewHolder.mSelectedOverlay.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
                        viewHolder.mEmo.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
                        viewHolder.mEmo.setImageResource(EmoProvider.getEmo(i));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            } else {
                viewHolder.mSelectedOverlay.setVisibility(View.INVISIBLE);
                viewHolder.mEmo.setVisibility(View.INVISIBLE);
                viewHolder.mEmo.setImageResource(EmoProvider.getEmo(i));
            }
        }

    }

    public void checkItem(int position) {
        if (position < mFiles.size()) {
            mSelectedPositions.put(position, !mSelectedPositions.get(position));
            notifyItemChanged(position);
            mAnimatedPositions.put(position, true);
            sendEvent();
        }
    }

    private void sendEvent() {
        if (mInfo == null)
            mInfo = new ImageAdapterInfo();

        mInfo.count = mFiles.size();
        mInfo.mSelectedFiles.clear();
        int i = 0;
        for (File file : mFiles) {
            boolean selected = mSelectedPositions.get(i);
            if (selected)
                mInfo.mSelectedFiles.add(file);
            i++;
        }
        mInfo.selected = mInfo.mSelectedFiles.size();
        EventBus.getDefault().post(mInfo);
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }
}
