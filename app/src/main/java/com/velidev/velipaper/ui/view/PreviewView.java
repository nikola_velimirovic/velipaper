package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import com.velidev.velipaper.event.EventDispatcher;
import com.velidev.velipaper.log.Logger;
import com.velidev.velipaper.service.DrawableInitialiser;

/**
 * Created by Nikola on 10.1.2015..
 */
public class PreviewView extends View {
    public static final int SOURCE_ID = 192;
    private Handler mHandler;

    private long mDrawInterval = 30;
    private Runnable mDrawRunnable = new Runnable() {
        @Override
        public void run() {
            postInvalidate();
            mHandler.postDelayed(this, mDrawInterval);
        }
    };
    private DrawableInitialiser mInitialiser;
    private Logger log = Logger.get(getClass());

    public PreviewView(Context context) {
        super(context);
        init();
    }

    private void init() {
        mInitialiser = new DrawableInitialiser(SOURCE_ID);
        mInitialiser.start();
        mHandler = new Handler();
        mHandler.post(mDrawRunnable);
    }

    public PreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        log.d("draw >> preview view");
        EventDispatcher.get(SOURCE_ID).sendTimeEvent();
        EventDispatcher.get(SOURCE_ID).sendDrawEvent(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        sendResizeEvent();
    }

    private void sendResizeEvent() {
        int height = getMeasuredHeight();
        int width = getMeasuredWidth();
        EventDispatcher.get(SOURCE_ID).sendReziseEvent(width, height);
    }

    public void reload() {
        init();
        sendResizeEvent();
        invalidate();
    }

    public void onDestroy() {
        mInitialiser.destroy();
    }
}
