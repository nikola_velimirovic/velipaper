package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Nikola on 25.1.2015..
 */
public class PlaceholderView extends LinearLayout implements View.OnClickListener {

    protected String mPlaceholderString;
    protected boolean mShowingPlaceholder = true;

    public PlaceholderView(Context context) {
        super(context);
    }

    public PlaceholderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlaceholderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PlaceholderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void showPlaceholder() {
        mShowingPlaceholder = true;
    }

    public void hidePlaceholder() {
        mShowingPlaceholder = false;
    }

    public void togglePlaceholder() {
        if (mShowingPlaceholder) {
            hidePlaceholder();
        } else {
            showPlaceholder();
        }
    }

    @Override
    public void onClick(View v) {
        togglePlaceholder();
    }
}
