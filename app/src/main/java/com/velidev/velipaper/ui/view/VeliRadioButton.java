package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.velidev.velipaper.event.UIEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 31.1.2015..
 */
public class VeliRadioButton extends RadioButton {

    private int mGroupId = -1;

    public VeliRadioButton(Context context) {
        super(context);
        init();
    }

    public VeliRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VeliRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public VeliRadioButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        if (checked) {
            int position = (int) getTag();
            EventBus.getDefault().post(new UIEvent(position, UIEvent.UIEventType.RADIO_CHECKED, mGroupId));
        }
    }

    public void onEvent(UIEvent event) {
        if (event.groupId != -1 && event.groupId == mGroupId && event.type == UIEvent.UIEventType.RADIO_CHECKED) {
            int position = (int) getTag();
            if (event.id != position)
                setChecked(false);
        }
    }

    public int getGroupId() {
        return mGroupId;
    }

    public void setGroupId(int groupId) {
        mGroupId = groupId;
    }
}
