package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ImagesPickedEvent;
import com.velidev.velipaper.event.UIEvent;
import com.velidev.velipaper.ui.adapter.ImagePickerAdapter;
import com.velidev.velipaper.util.Prefs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 6.1.2015..
 */
public class ImagePickerToolbar extends Toolbar implements View.OnClickListener {

    private TextView mTitle;
    private View mTick;
    private ImagePickerAdapter.ImageAdapterInfo mInfo;

    public ImagePickerToolbar(Context context) {
        super(context);
        init();
    }

    public ImagePickerToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        EventBus.getDefault().register(this);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View root = inflater.inflate(R.layout.toolbar_image_picker, this);
        mTitle = (TextView) root.findViewById(R.id.image_picker_toolbar_title);
        mTick = root.findViewById(R.id.image_picker_check);
        mTick.setOnClickListener(this);
        update();
    }

    public void onEvent(ImagePickerAdapter.ImageAdapterInfo info) {
        mInfo = info;
        update();
    }

    private void update() {
        if (mInfo != null && mInfo.selected > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Selected ");
            stringBuilder.append("<b>");
            stringBuilder.append(mInfo.selected);
            stringBuilder.append("</b>");
            stringBuilder.append(" out of ");
            stringBuilder.append(mInfo.count);
            Spanned spanned = Html.fromHtml(stringBuilder.toString());
            mTitle.setText(spanned);
            if (mTick.getVisibility() != VISIBLE) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.push_left_in);
                animation.setDuration(250);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mTick.setVisibility(VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mTick.startAnimation(animation);
            } else {
                mTick.clearAnimation();
                mTick.setVisibility(VISIBLE);
            }
        } else {
            mTitle.setText(R.string.image_picker_toolbar_placeholder);
            if (mTick.getVisibility() == VISIBLE) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.push_left_out);
                animation.setDuration(250);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mTick.setVisibility(INVISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mTick.startAnimation(animation);
            } else {
                mTick.setVisibility(INVISIBLE);
            }
        }
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        List<String> paths = new ArrayList<>();
        for (File file : mInfo.mSelectedFiles) {
            paths.add(file.getAbsolutePath());
        }

        EventBus.getDefault().post(new ImagesPickedEvent(paths));
    }
}
