package com.velidev.velipaper.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.velidev.velipaper.R;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.event.ColorPickedEvent;
import com.velidev.velipaper.util.EmoProvider;
import com.velidev.velipaper.util.Prefs;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 2/9/2015.
 */
public class ColorPickerFragment extends VeliFragment implements ColorPicker.OnColorChangedListener {

    @InjectView(R.id.picker)
    ColorPicker mPicker;

    @InjectView(R.id.color_tick)
    ImageButton mTick;

    @Inject
    @Named("scaleIn")
    Animation mScaleInAnimation;

    @Inject
    @Named("scaleOut")
    Animation mScaleOutAnimation;

    boolean mChangedToTick = false;

    int mColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_color_picker, null);
        ButterKnife.inject(this, root);
        mPicker.setOnColorChangedListener(this);
        int color = Prefs.getPrefs().getColor();
        mColor = color == -1 ? getResources().getColor(R.color.purple_ascent_color) : color;
        mPicker.setColor(mColor);
        mTick.setImageResource(EmoProvider.getEmo(1));
        mScaleOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mTick.setImageResource(R.drawable.ic_action_tick);
                mTick.startAnimation(mScaleInAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return root;
    }

    @OnClick(R.id.color_tick)
    public void saveColor() {
        getFragmentManager().popBackStackImmediate();
        EventBus.getDefault().post(new ColorPickedEvent(mColor));
    }

    @Override
    public void onColorChanged(int i) {
        mColor = i;
        if (!mChangedToTick) {
            mChangedToTick = true;
            mTick.startAnimation(mScaleOutAnimation);
        }
    }
}
