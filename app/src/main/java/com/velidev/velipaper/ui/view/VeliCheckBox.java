package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.velidev.velipaper.R;
import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.di.annotation.Named;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Nikola on 25.1.2015..
 */
public class VeliCheckBox extends PlaceholderView implements CompoundButton.OnCheckedChangeListener {

    @InjectView(R.id.veli_checkbox)
    CheckBox mCheckBox;
    @InjectView(R.id.veli_seek_bar_placeholder)
    TextView mPlaceholder;
    @Inject
    @Named("fadeIn")
    Animation mFadeInAnimation;
    @Inject
    @Named("fadeOut")
    Animation mFadeOutAnimation;
    private String mPlaceholderString;
    private boolean mChecked;
    private String mText;
    private CompoundButton.OnCheckedChangeListener mCheckedChangeListener;

    public VeliCheckBox(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View root = inflater.inflate(R.layout.veli_checkbox, this);
        ButterKnife.inject(this, root);
        VeliApplication.inject(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VeliComboBox);
            mText = a.getString(R.styleable.VeliCheckBox_checkbox_text);
            mPlaceholderString = a.getString(R.styleable.VeliCheckBox_checkbox_placeholder);
            mChecked = a.getBoolean(R.styleable.VeliCheckBox_checked, false);
            a.recycle();
        }

        mCheckBox.setChecked(mChecked);
        mPlaceholder.setText(mPlaceholderString);
        mCheckBox.setText(mText);
        mCheckBox.setOnCheckedChangeListener(this);
        mCheckBox.setVisibility(INVISIBLE);
        mPlaceholder.setVisibility(VISIBLE);
    }

    public VeliCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public VeliCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public VeliCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    @Override
    public void showPlaceholder() {
        mShowingPlaceholder = true;
        mCheckBox.startAnimation(mFadeOutAnimation);
        mPlaceholder.startAnimation(mFadeInAnimation);
    }

    @Override
    public void hidePlaceholder() {
        mShowingPlaceholder = false;
        mCheckBox.startAnimation(mFadeInAnimation);
        mPlaceholder.startAnimation(mFadeOutAnimation);
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener checkedChangeListener) {
        mCheckedChangeListener = checkedChangeListener;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (mCheckedChangeListener != null)
            mCheckedChangeListener.onCheckedChanged(buttonView, isChecked);
    }
}
