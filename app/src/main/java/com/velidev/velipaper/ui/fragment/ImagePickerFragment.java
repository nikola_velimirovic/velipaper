package com.velidev.velipaper.ui.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.ImagesPickedEvent;
import com.velidev.velipaper.log.Logger;
import com.velidev.velipaper.ui.adapter.ImagePickerAdapter;
import com.velidev.velipaper.util.ItemClickSupport;
import com.velidev.velipaper.util.VeliPaperUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 5.1.2015..
 */
public class ImagePickerFragment extends Fragment implements ItemClickSupport.OnItemClickListener {

    @InjectView(R.id.image_picker_grid)
    RecyclerView mRecyclerView;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;
    private ImagePickerAdapter mAdapter;
    private List<File> mFiles;
    private AsyncTask<Void, File, Void> mLoadTask;
    private Logger log = Logger.get(getClass());

    public ImagePickerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_image_picker, null);
        ButterKnife.inject(this, root);
        mRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        int padding = getResources().getDimensionPixelSize(R.dimen.image_picker_padding);
        final int columnCount = getResources().getInteger(R.integer.column_count);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), columnCount);
        mRecyclerView.setPadding(0, 0, padding, 0);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter == null)
                    mAdapter = new ImagePickerAdapter(getActivity(), new ArrayList<File>(), mRecyclerView.getWidth());
                else
                    mAdapter.reset(getActivity(), mRecyclerView.getWidth());
                mRecyclerView.setAdapter(mAdapter);
                loadImages(mRecyclerView.getWidth() / columnCount);
            }
        });
        ItemClickSupport clickSupport = ItemClickSupport.addTo(mRecyclerView);
        clickSupport.setOnItemClickListener(this);
        return root;
    }

    private void loadImages(final int size) {
        if (mFiles == null) {
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadTask = new AsyncTask<Void, File, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    mFiles = VeliPaperUtil.getSDCardImagesPath(getActivity(), true);
                    for (final File file : mFiles) {
                        try {
                            Bitmap bitmap = Picasso.with(getActivity()).load(file).placeholder(R.color.transparent_purple).resize(size, size).centerCrop().get();
                            if (bitmap != null) {
                                publishProgress(file);
                            }
                        } catch (Exception e) {
                            log.e("error at load task", e);
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (mProgressBar != null)
                        mProgressBar.setVisibility(View.GONE);

                }

                @Override
                protected void onProgressUpdate(File... values) {
                    mAdapter.add(values[0]);
                }
            };
            mLoadTask.execute((Void[]) null);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mLoadTask != null) {
            mLoadTask.cancel(true);
        }
    }

    public void onEvent(ImagesPickedEvent event) {
        getFragmentManager().popBackStackImmediate();
    }

    /**
     * Callback method to be invoked when an item in the RecyclerView
     * has been clicked.
     *
     * @param parent   The RecyclerView where the click happened.
     * @param view     The view within the RecyclerView that was clicked
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(RecyclerView parent, View view, int position, long id) {
        if (mAdapter != null) {
            mAdapter.checkItem(position);
        }
    }
}
