package com.velidev.velipaper.ui.fragment;

import android.support.v4.app.Fragment;

import com.velidev.velipaper.VeliApplication;

/**
 * Created by Nikola on 2/14/2015.
 */
public abstract class VeliFragment extends Fragment {


    protected VeliFragment() {
        VeliApplication.inject(this);
    }
}
