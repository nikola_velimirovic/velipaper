package com.velidev.velipaper.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.velidev.velipaper.R;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.event.ColorPickedEvent;
import com.velidev.velipaper.event.ImagesPickedEvent;
import com.velidev.velipaper.ui.view.PreviewView;
import com.velidev.velipaper.ui.view.VeliCheckBox;
import com.velidev.velipaper.ui.view.VeliComboBox;
import com.velidev.velipaper.ui.view.VeliSeekBar;
import com.velidev.velipaper.util.Prefs;
import com.velidev.velipaper.util.VeliPaperUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 10.1.2015..
 */
public class PreviewFragment extends VeliFragment implements View.OnClickListener, VeliComboBox.VeliComboBoxListener {

    @InjectView(R.id.settings_overlay)
    View mSettingsOverlay;
    @InjectView(R.id.preview_clock)
    View mClockIcon;
    @InjectView(R.id.preview_brush)
    View mBackgroundIcon;
    @InjectView(R.id.preview_rss_text)
    View mTextIcon;
    @InjectView(R.id.preview_blur)
    View mBlurIcon;
    @InjectView(R.id.preview_timer)
    View mTimerIcon;
    @InjectView(R.id.preview_clock_title)
    View mClockTitle;
    @InjectView(R.id.preview_background_title)
    View mBackgroundTitle;
    @InjectView(R.id.preview_buttons_holder)
    View mButtonHolder;
    @InjectView(R.id.toolbar)
    View mToolbar;
    @InjectView(R.id.image_picker_check)
    View mCheckmark;
    @InjectView(R.id.preview_view)
    PreviewView mPreviewView;
    @InjectView(R.id.preview_settings)
    ImageButton mSettingsButton;
    @InjectView(R.id.preview_timer_seek)
    VeliSeekBar mTimerSeekBar;
    @InjectView(R.id.preview_blur_seek)
    VeliSeekBar mBlurSeekBar;
    @InjectView(R.id.preview_rss_checkbox)
    VeliCheckBox mRSSCheckbox;
    @InjectView(R.id.clock_checkbox)
    VeliComboBox mClockComboBox;
    @InjectView(R.id.background_checkbox)
    VeliComboBox mBackgroundCombobox;

    @InjectView(R.id.settings_hint)
    TextView mSettingsHint;
    boolean mButtonsVisible = false;
    boolean mAnimating = false;
    @Inject
    @Named("pushTopIn")
    Animation mToolbarInAnimation;
    @Inject
    @Named("pushTopOut")
    Animation mToolbarOutAnimation;
    @Inject
    @Named("scaleIn")
    Animation mScaleInAnimation;
    @Inject
    @Named("scaleOut")
    Animation mScaleOutAnimation;
    @Inject
    @Named("fadeIn")
    Animation mFadeInAnimation;
    @Inject
    @Named("fadeOut")
    Animation mFadeOutAnimation;
    @Inject
    @Named("pushBottomIn")
    Animation mOpenAnimation;
    @Inject
    @Named("pushBottomOut")
    Animation mCloseAnimation;
    private boolean mFromSettingsScreen;
    private boolean mImagesChanged = false;
    private boolean mColorChanged = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnimating = true;
                mButtonHolder.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mCloseAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnimating = false;
                mButtonHolder.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_preview, null);
        ButterKnife.inject(this, root);
        mCheckmark.setOnClickListener(this);
        mSettingsButton.setOnClickListener(this);
        mClockIcon.setOnClickListener(this);
        mBackgroundIcon.setOnClickListener(this);
        mBlurIcon.setOnClickListener(mBlurSeekBar);
        mTimerIcon.setOnClickListener(mTimerSeekBar);
        mTextIcon.setOnClickListener(mRSSCheckbox);

        mClockComboBox.setOptionsResource(R.array.clock_types);
        int clockMode = Prefs.getPrefs().getClockMode();
        mClockComboBox.select(clockMode);
        mClockComboBox.setComboBoxListener(this);
        mBackgroundCombobox.setOptionsResource(R.array.background_types);
        mBackgroundCombobox.postDelayed(new Runnable() {
            @Override
            public void run() {
                int backgroundMode = Prefs.getPrefs().getBackgroundMode();
                mBackgroundCombobox.select(backgroundMode);


            }
        }, 300);
        mTimerSeekBar.setProgress(Prefs.getPrefs().getTransitionTime());
        mBlurSeekBar.setProgress(Prefs.getPrefs().getBlur());
        mBackgroundCombobox.setComboBoxListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preview_settings:
                if (mAnimating) return;
                if (mButtonsVisible) {
                    hideMenu();
                } else {
                    showMenu();
                }
                break;
            case R.id.preview_clock:
                mClockComboBox.toggleVisibility();
                break;
            case R.id.preview_brush:
                mBackgroundCombobox.toggleVisibility();
                break;
            case R.id.image_picker_check:
                if (!mFromSettingsScreen) {
                    VeliPaperUtil.startWallpaperChooser(getActivity());
                    Toast.makeText(getActivity(), "Press set it up or something", Toast.LENGTH_LONG).show();
                }
                getActivity().finish();
                break;
        }
    }

    public void hideMenu() {
        mScaleOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                hideButtons(true);
                mSettingsButton.setImageResource(R.drawable.ic_action_gear);
                mSettingsButton.startAnimation(mScaleInAnimation);
                mSettingsHint.startAnimation(mFadeOutAnimation);
                //TODO provera da li se desila neka razlika :P

                int blur = mBlurSeekBar.getProgress();
                int interval = mTimerSeekBar.getProgress();
                int clockOption = mClockComboBox.getCheckedPosition();
                final int backgroundOption = mBackgroundCombobox.getCheckedPosition();

                boolean blurChanged = Prefs.getPrefs().setBlur(blur);
                boolean intervalChanged = Prefs.getPrefs().setTransitionTime(interval);
                boolean clockChanged = Prefs.getPrefs().setClockMode(clockOption);
                boolean backgroundChanged = Prefs.getPrefs().setBackgroundMode(backgroundOption);

                if (mImagesChanged || mColorChanged || blurChanged || intervalChanged || clockChanged || backgroundChanged) {
                    mImagesChanged = false;
                    mColorChanged = false;
                    mPreviewView.reload();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mSettingsButton.startAnimation(mScaleOutAnimation);
    }

    private void showMenu() {
        mScaleOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                showButtons();
                mSettingsButton.setImageResource(R.drawable.ic_action_tick);
                mSettingsButton.startAnimation(mScaleInAnimation);
                mSettingsHint.startAnimation(mFadeInAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mSettingsButton.startAnimation(mScaleOutAnimation);
    }

    private void hideButtons(boolean showToolbar) {
        mButtonsVisible = false;
        mButtonHolder.startAnimation(mCloseAnimation);
        mSettingsOverlay.startAnimation(mFadeOutAnimation);
        mBackgroundTitle.startAnimation(mFadeOutAnimation);
        mClockTitle.startAnimation(mFadeOutAnimation);
        if (showToolbar)
            mToolbar.startAnimation(mToolbarInAnimation);
    }

    private void showButtons() {
        mButtonsVisible = true;
        mButtonHolder.startAnimation(mOpenAnimation);
        mSettingsOverlay.startAnimation(mFadeInAnimation);
        mBackgroundTitle.startAnimation(mFadeInAnimation);
        mClockTitle.startAnimation(mFadeInAnimation);
        mToolbar.startAnimation(mToolbarOutAnimation);
    }

    @Override
    public void onClicked(int groupId, int position) {
        if (groupId == getResources().getInteger(R.integer.background_group)) {
            switch (position) {
                //slike su prve
                case 0:
                    openImagePickerFragment();
                    break;
                //color
                case 1:
                    openColorPickerFragment();
                    break;
                //rss

                case 2:

                    break;
            }
        }
    }

    public void openImagePickerFragment() {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.preview_fragment, new ImagePickerFragment());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void openColorPickerFragment() {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.preview_fragment, new ColorPickerFragment());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onChecked(int groupId, int position) {
    }

    public void onEvent(ImagesPickedEvent event) {
        mImagesChanged = Prefs.getPrefs().setImages(event.paths);
    }

    public void onEvent(ColorPickedEvent event) {
        mColorChanged = Prefs.getPrefs().setColor(event.color);
    }

    public boolean isFromSettingsScreen() {
        return mFromSettingsScreen;
    }

    public void setFromSettingsScreen(boolean fromSettingsScreen) {
        mFromSettingsScreen = fromSettingsScreen;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPreviewView.onDestroy();
    }
}
