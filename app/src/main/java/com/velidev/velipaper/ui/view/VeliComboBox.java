package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.event.UIEvent;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 31.1.2015..
 */
public class VeliComboBox extends LinearLayout implements View.OnClickListener {

    @InjectView(R.id.root)
    LinearLayout mRoot;
    private Animation sFadeInAnimation;
    private Animation sFadeOutAnimation;
    private int mCheckedPosition = 0;
    private String[] mOptions;
    private int mGroupId;
    private VeliComboBoxListener mListener;
    private boolean mVisible = false;

    public VeliComboBox(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View root = inflater.inflate(R.layout.veli_combobox, this);
        ButterKnife.inject(this, root);
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VeliComboBox);
            mGroupId = a.getInteger(R.styleable.VeliComboBox_combo_group, -1);
            if (mGroupId == -1)
                throw new IllegalArgumentException("Group id must be set on VeliComboBox");
            a.recycle();
        }

        sFadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        sFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mRoot.setVisibility(INVISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRoot.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        sFadeInAnimation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        sFadeInAnimation.setFillAfter(true);

        sFadeOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        sFadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mRoot.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRoot.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        sFadeOutAnimation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        sFadeOutAnimation.setFillAfter(true);

        mRoot.setVisibility(GONE);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    public VeliComboBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public VeliComboBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public VeliComboBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void select(int position) {
        View view = mRoot.getChildAt(position);
        VeliRadioButton radio = (VeliRadioButton) view.findViewById(R.id.combobox_option_radio);
        radio.setChecked(true);
        mCheckedPosition = position;
    }

    public int getCheckedPosition() {
        return mCheckedPosition;
    }

    public VeliComboBoxListener getComboBoxListener() {
        return mListener;
    }

    public void setComboBoxListener(VeliComboBoxListener listener) {
        mListener = listener;
    }

    public void setOptionsResource(int optionsResource) {
        mOptions = getResources().getStringArray(optionsResource);
        initOptions();
    }

    private void initOptions() {
        mRoot.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < mOptions.length; i++) {
            String option = mOptions[i];
            LinearLayout view = (LinearLayout) inflater.inflate(R.layout.veli_combobox_option, mRoot, false);
            TextView title = (TextView) view.findViewById(R.id.combobox_option_title);
            VeliRadioButton radio = (VeliRadioButton) view.findViewById(R.id.combobox_option_radio);
            title.setText(option);
            title.setOnClickListener(this);
            radio.setOnClickListener(this);
            title.setTag(i);
            radio.setTag(i);
            radio.setGroupId(mGroupId);
            mRoot.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (mListener != null) {
            mListener.onClicked(mGroupId, position);
        }
    }

    public void onEvent(UIEvent event) {
        //ovo proverava da lis e neki drugi view otkrio a da nije ovaj view
        if (event.type == UIEvent.UIEventType.SHOW_OPTION && event.id != getId() && mVisible) {
            hide();
        }
        if (mListener != null && mGroupId == event.groupId && event.type == UIEvent.UIEventType.RADIO_CHECKED) {
            mListener.onChecked(mGroupId, event.id);
            mCheckedPosition = event.id;
        }
    }

    public void hide() {
        mVisible = false;
        mRoot.setVisibility(GONE);
        mRoot.startAnimation(sFadeOutAnimation);
    }

    public void toggleVisibility() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    public void show() {
        mVisible = true;
        mRoot.setVisibility(VISIBLE);
        mRoot.startAnimation(sFadeInAnimation);
        //ovo se stavlja na show jer obavestavamo druge viewe da se otkrio view
        EventBus.getDefault().post(new UIEvent(getId(), UIEvent.UIEventType.SHOW_OPTION));
    }

    public static interface VeliComboBoxListener {

        public void onClicked(int groupId, int position);

        public void onChecked(int groupId, int position);
    }
}
