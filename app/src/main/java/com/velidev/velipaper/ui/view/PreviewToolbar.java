package com.velidev.velipaper.ui.view;

import android.content.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;

import com.velidev.velipaper.*;

/**
 * Created by Nikola on 10.1.2015..
 */
public class PreviewToolbar extends Toolbar {

    private View mRoot;

    public PreviewToolbar(Context context) {
        super(context);
        init();
    }


    public PreviewToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private void init() {
        mRoot = LayoutInflater.from(getContext()).inflate(R.layout.toolbar_preview, this);
    }
}
