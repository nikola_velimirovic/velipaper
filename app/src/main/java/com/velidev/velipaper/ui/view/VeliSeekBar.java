package com.velidev.velipaper.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.velidev.velipaper.R;
import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.event.UIEvent;
import com.velidev.velipaper.util.Prefs;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 25.1.2015..
 */
public class VeliSeekBar extends PlaceholderView implements SeekBar.OnSeekBarChangeListener {

    @InjectView(R.id.veli_seek_bar_value)
    TextView mStatus;
    @InjectView(R.id.veli_seek_bar_placeholder)
    TextView mPlaceholder;
    @InjectView(R.id.veli_seek_bar)
    SeekBar mSeekBar;
    @InjectView(R.id.veli_seek_bar_holder)
    LinearLayout mHolder;
    @Inject
    @Named("fadeIn")
    Animation mFadeInAnimation;
    @Inject
    @Named("fadeOut")
    Animation mFadeOutAnimation;

    private int mMin;
    private VeliSeekBarProgressListener mListener;
    private int mSufixId = R.string.veli_seekbar_min_sufix;
    private int mMax;

    public VeliSeekBar(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View root = inflater.inflate(R.layout.veli_seek_bar, this);
        ButterKnife.inject(this, root);
        VeliApplication.inject(this);
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VeliSeekBar);
            mSufixId = a.getResourceId(R.styleable.VeliSeekBar_value_text, -1);
            mPlaceholderString = a.getString(R.styleable.VeliSeekBar_placeholder);
            mMax = a.getInteger(R.styleable.VeliSeekBar_max, Prefs.MAX_TRANSITION_TIME);
            mMin = a.getInteger(R.styleable.VeliSeekBar_min, Prefs.MAX_TRANSITION_TIME);
            a.recycle();
        }
        mSeekBar.setOnSeekBarChangeListener(this);
        init(mMin, mMax, 0);
        setPlaceholder(mPlaceholderString);
        mHolder.setVisibility(INVISIBLE);
        mPlaceholder.setVisibility(VISIBLE);
        EventBus.getDefault().register(this);
    }

    public void init(int min, int max, int value) {
        mMin = min;
        mSeekBar.setMax(max);
        mSeekBar.setProgress(value);
    }

    public void setPlaceholder(String string) {
        mPlaceholderString = string;
        if (!TextUtils.isEmpty(mPlaceholderString)) {
            mPlaceholder.setText(mPlaceholderString);
        } else {
            mPlaceholder.setVisibility(View.GONE);
        }
    }

    public VeliSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public VeliSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public VeliSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void onEvent(UIEvent event) {
        if (event.type == UIEvent.UIEventType.SHOW_OPTION && event.id != getId() && !mShowingPlaceholder) {
            showPlaceholder();
        }
    }

    @Override
    public void showPlaceholder() {
        super.showPlaceholder();
        mHolder.startAnimation(mFadeOutAnimation);
        mPlaceholder.startAnimation(mFadeInAnimation);
    }

    @Override
    public void hidePlaceholder() {
        super.hidePlaceholder();
        mHolder.startAnimation(mFadeInAnimation);
        mPlaceholder.startAnimation(mFadeOutAnimation);
        EventBus.getDefault().post(new UIEvent(getId(), UIEvent.UIEventType.SHOW_OPTION));
    }

    public void setSufix(int res) {
        mSufixId = res;
    }

    public void removeSufix() {
        mSufixId = -1;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress >= mMin) {
            if (mSufixId != -1) {
                mStatus.setText(getResources().getString(mSufixId, progress));
            } else {
                mStatus.setText(Integer.toString(progress));
            }
            if (mListener != null)
                mListener.onProgress(getId(), progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public VeliSeekBarProgressListener getProgressListener() {
        return mListener;
    }

    public void setProgressListener(VeliSeekBarProgressListener listener) {
        mListener = listener;
    }

    public int getProgress() {
        int progress = mSeekBar.getProgress();
        if (progress < mMin)
            progress = mMin;

        if (progress > mMax)
            progress = mMax;

        return progress;
    }

    public void setProgress(int progress) {
        mSeekBar.setProgress(progress);
    }

    public static interface VeliSeekBarProgressListener {

        public void onProgress(int id, int progress);

    }
}
