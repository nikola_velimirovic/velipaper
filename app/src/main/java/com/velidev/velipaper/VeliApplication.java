package com.velidev.velipaper;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import com.velidev.velipaper.di.module.AnimationProviderModule;
import com.velidev.velipaper.di.module.ColorProviderModule;
import com.velidev.velipaper.util.VeliPaperUtil;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by Nikola on 10.1.2015..
 */
public class VeliApplication extends Application {

    public static Context sContext;
    private static VeliApplication sInstance;
    private ObjectGraph sObjectGraph;

    public static void inject(Object object) {
        sInstance.sObjectGraph.inject(object);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sContext = getApplicationContext();
        sObjectGraph = ObjectGraph.create(getModules().toArray());
        refreshSdCardPaths();
    }

    protected List<Object> getModules() {
        return Arrays.asList(
                new ColorProviderModule(this),
                new AnimationProviderModule(this)
        );
    }

    private void refreshSdCardPaths() {
        AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                VeliPaperUtil.getSDCardImagesPath(sContext, false);
                return null;
            }
        };

        asyncTask.execute((Void[]) null);
    }

}
