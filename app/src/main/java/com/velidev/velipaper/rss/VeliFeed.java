package com.velidev.velipaper.rss;

import org.mcsoxford.rss.*;

/**
 * Created by Nikola on 2.1.2015..
 */
public class VeliFeed {

    private final String mUrl;
    private RSSFeed mFeed;
    private int mPosition = 0;

    public VeliFeed(String url) {
        mUrl = url;
    }

    public void setFeed(RSSFeed feed) {
        mFeed = feed;
    }

    public String getUrl(){
        return mUrl;
    }

    public RSSItem next() {
        if (mFeed == null || mFeed.getItems().size() == 0)
            return null;
        try {
            return mFeed.getItems().get(mPosition);
        } catch (Exception e) {
            mPosition = 0;
            return mFeed.getItems().get(mPosition);
        } finally {
            mPosition++;
        }

    }

}
