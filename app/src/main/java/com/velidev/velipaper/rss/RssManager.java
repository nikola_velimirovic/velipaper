package com.velidev.velipaper.rss;

import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.velidev.velipaper.util.Prefs;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.mcsoxford.rss.MediaEnclosure;
import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;

import java.util.List;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;

/**
 * Created by Nikola on 1.1.2015..
 */
public class RssManager {

    private class LoadTask extends AsyncTask<Void, Void, RSSFeed> {

        private String mUrl;

        public void execute(final String url) {
            mUrl = url;
            execute((Void[]) null);
        }

        @Override
        protected RSSFeed doInBackground(Void... params) {
            RSSReader reader = new RSSReader();
            try {
                RSSFeed feed = reader.load(mUrl);
                List<RSSItem> items = feed.getItems();
                for (RSSItem item : items) {
                    if (item.getEnclosure() == null || TextUtils.isEmpty(item.getEnclosure().getUrl().toString())) {
                        Document document = Jsoup.parse(item.getDescription());
                        Elements img = document.getElementsByTag("img");
                        String src = img.attr("src");
                        item.setEnclosure(new MediaEnclosure(Uri.parse(src), 12, ""));
                    }
                }

                return feed;
            } catch (Exception e) {
            }
            return null;
        }

        @DebugLog
        @Override
        protected void onPostExecute(RSSFeed rssFeed) {
            if (rssFeed != null) {
                try {
                    for (VeliFeed feed : mFeeds) {
                        if (feed.getUrl().equals(mUrl)) {
                            feed.setFeed(rssFeed);
                            mInitialised = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    private VeliFeed[] mFeeds;
    private long mUpdateTime = 0;
    private int mUpdatePosition = 0;
    private boolean mInitialised = false;

    public RssManager() {
        List<String> urls = Prefs.getPrefs().getFeeds();
        if (urls != null && urls.size() > 0) {
            mFeeds = new VeliFeed[urls.size()];
            int i = 0;
            for (String url : urls) {
                mFeeds[i] = new VeliFeed(url);
                load(url);
                i++;
            }
        }
    }

    public void update() {
        //TODO update one koji su istekli

        //TODO proveriti koliko je proslo i ako je proslo dovoljno saljemo sledeci feed
        if (mInitialised && System.currentTimeMillis() > (mUpdateTime + 100000)) {
            int position = mUpdatePosition % mFeeds.length;
            RSSItem item = mFeeds[position].next();
            if (item != null) {
                mUpdatePosition++;
                EventBus.getDefault().post(item);
            }
            mUpdateTime = System.currentTimeMillis();
        }
    }

    @DebugLog
    public void load(String feedUrl) {
        LoadTask loadTask = new LoadTask();
        loadTask.execute(feedUrl);
    }
}
