package com.velidev.velipaper.service;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Canvas;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.event.EventDispatcher;
import com.velidev.velipaper.log.Logger;
import com.velidev.velipaper.util.Prefs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;


/**
 * This is the main live wallpaper drawing service.
 * Created by Nikola on 8.12.2014..
 */

public class VeliPaperService extends WallpaperService {

    public static final int SOURCE_ID = 191;
    public static final int ANIMATION_TIME = 10000;
    private static final int MS_PER_FRAME = 100;
    public static Context context;
    @Inject
    @Named("VeliPaperService")
    Logger log;

    @Override
    public void onCreate() {
        super.onCreate();
        VeliApplication.inject(this);
        Config.DEBUG = (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
        context = getApplicationContext();
        List<String> feeds = new ArrayList<>();
        feeds.add("http://www.kurir.rs/rss/vesti/");
        feeds.add("http://9gag-rss.com/api/rss/get?code=9GAG&format=2");
        Prefs.getPrefs().setFeeds(feeds);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        context = null;
    }

    /**
     * Must be implemented to return a new instance of the wallpaper's engine.
     * Note that multiple instances may be active at the same time, such as
     * when the wallpaper is currently set as the active wallpaper and the user
     * is in the wallpaper picker viewing a preview of it as well.
     */
    @Override
    public Engine onCreateEngine() {
        return new VeliPaperServiceEngine();
    }

    /**
     * VeliPaperServiceEngine - main class for drawing our live wallpaper.
     */
    public class VeliPaperServiceEngine extends WallpaperService.Engine {

        //        private final RssManager mManager;
        private Handler mHandler;
        private boolean mVisible = false;
        private Runnable mDrawRunner = new Runnable() {
            @Override
            public void run() {
                doDraw();
            }
        };

        public VeliPaperServiceEngine() {
            mHandler = new Handler();
//            mManager = new RssManager();
            mDrawableInitialiser.start();
        }        //TODO make this dinamic

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;
            if (mVisible) {
                mHandler.post(mDrawRunner);
            } else {
                mHandler.removeCallbacks(mDrawRunner);
            }
        }

        @DebugLog
        @Override
        public void onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
//            EventBus.getDefault().post(new OffsetEvent(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset));
        }

        @DebugLog
        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            EventDispatcher.get(SOURCE_ID).sendReziseEvent(width, height);
            super.onSurfaceChanged(holder, format, width, height);
        }

        private DrawableInitialiser mDrawableInitialiser = new DrawableInitialiser(SOURCE_ID);

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            mVisible = false;
            mHandler.removeCallbacks(mDrawRunner);
        }

        private void doDraw() {
            EventDispatcher.get(SOURCE_ID).sendTimeEvent();
            SurfaceHolder holder = getSurfaceHolder();
            Canvas canvas = null;
            try {
                canvas = holder.lockCanvas();
                draw(canvas);
            } finally {
                if (canvas != null)
                    holder.unlockCanvasAndPost(canvas);
            }
            mHandler.removeCallbacks(mDrawRunner);
            if (mVisible) {
                mHandler.postDelayed(mDrawRunner, MS_PER_FRAME);
            }
        }

        @DebugLog
        private void draw(Canvas canvas) {
            EventDispatcher.get(SOURCE_ID).sendDrawEvent(canvas);
        }


    }

}
