package com.velidev.velipaper.service;

import android.graphics.*;
import android.text.*;

/**
 * Created by Nikola on 30.12.2014..
 */
public class PaintProvider {

    private static Paint sWhiteShadowPaint;
    private static TextPaint sWhiteShadowTextPaint;

    public static Paint getWhiteShadowPaint() {
        if (sWhiteShadowPaint == null) {
            sWhiteShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            sWhiteShadowPaint.setColor(Color.WHITE);
            sWhiteShadowPaint.setShadowLayer(5.0f, 0.0f, 3.0f, Color.argb(140, 0, 0, 0));
        }
        sWhiteShadowPaint.setTextAlign(Paint.Align.LEFT);
        return sWhiteShadowPaint;
    }

    public static TextPaint getWhiteShadowTextPaint() {
        if (sWhiteShadowTextPaint == null) {
            sWhiteShadowTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            sWhiteShadowTextPaint.setColor(Color.WHITE);
            sWhiteShadowTextPaint.setShadowLayer(5.0f, 0.0f, 3.0f, Color.argb(140, 0, 0, 0));
            sWhiteShadowTextPaint.setTextSize(20);
            sWhiteShadowTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        }
        sWhiteShadowTextPaint.setTextAlign(Paint.Align.CENTER);
        return sWhiteShadowTextPaint;
    }


}
