package com.velidev.velipaper.service;

import com.velidev.velipaper.drawable.AnalogClockDrawable;
import com.velidev.velipaper.drawable.BitmapBackgroundDrawable;
import com.velidev.velipaper.drawable.CircularDateDrawable;
import com.velidev.velipaper.drawable.ColorBackgroundDrawable;
import com.velidev.velipaper.drawable.RssBackgroundDrawable;
import com.velidev.velipaper.drawable.SaturnClockDrawable;
import com.velidev.velipaper.drawable.TransitionDrawable;
import com.velidev.velipaper.drawable.VeliDrawable;
import com.velidev.velipaper.event.DrawEvent;
import com.velidev.velipaper.event.GenericEvent;
import com.velidev.velipaper.event.TimeEvent;
import com.velidev.velipaper.util.Prefs;

import de.greenrobot.event.EventBus;

/**
 * Created by Nikola on 21.12.2014..
 */
public class DrawableInitialiser {

    private final int mSourceId;
    private VeliDrawable mBackgroundDrawable;
    private VeliDrawable mCircularDateDrawable;
    private VeliDrawable mClockDrawable;
    private boolean mDirty = true;
    private int mDrawMinute = -1;
    private TransitionDrawable mTransitionDrawable;

    public DrawableInitialiser(int sourceId) {
        mSourceId = sourceId;
        EventBus.getDefault().register(this);
    }

    public void onEvent(DrawEvent event) {
        //TODO check if dirty
//        if (mDirty) {
        mBackgroundDrawable.onDrawEvent(event);
        mCircularDateDrawable.onDrawEvent(event);
        mClockDrawable.onDrawEvent(event);
        mTransitionDrawable.onDrawEvent(event);
//            mDirty = false;
//        }
    }

    public void destroy() {
        mBackgroundDrawable = null;
        mCircularDateDrawable = null;
        mClockDrawable = null;
        mTransitionDrawable = null;
    }

    public void onEvent(TimeEvent event) {
        if (event.getSourceId() == mSourceId) {
            if (mDrawMinute != event.mMinute) {
                mDirty = true;
            }
        }
    }

    public void onEvent(GenericEvent event) {
        if (event.getMessage() == GenericEvent.REQUEST_UPDATE) {
            mTransitionDrawable.startTransition();
        }
    }

    public void reload() {
        start();
    }

    public void start() {
        int clockMode = Prefs.getPrefs().getClockMode();
        int backgroundMode = Prefs.getPrefs().getBackgroundMode();

        //set up background
        switch (backgroundMode) {
            case Prefs.BACKGROUND_MODE_COLOR:
                mBackgroundDrawable = new ColorBackgroundDrawable(mSourceId);
                break;
            case Prefs.BACKGROUND_MODE_RSS:
                mBackgroundDrawable = new RssBackgroundDrawable(mSourceId);
                break;
            case Prefs.BACKGROUND_MODE_IMAGES:
                mBackgroundDrawable = new BitmapBackgroundDrawable(mSourceId);
                break;
        }

        mCircularDateDrawable = new CircularDateDrawable(mSourceId);

        //setting up clock
        if (clockMode == Prefs.CLOCK_MODE_SATURN) {
            mClockDrawable = new SaturnClockDrawable(mSourceId);
        } else {
            mClockDrawable = new AnalogClockDrawable(mSourceId);
        }

        mTransitionDrawable = new TransitionDrawable(mSourceId);
    }

    public boolean isDirty() {
        return mDirty;
    }

    public void setDirty(boolean dirty) {
        mDirty = dirty;
    }
}
