package com.velidev.velipaper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.velidev.velipaper.R;
import com.velidev.velipaper.ui.fragment.PreviewFragment;
import com.velidev.velipaper.di.module.ColorProviderModule;

import dagger.ObjectGraph;

/**
 * Created by Nikola on 8.12.2014..
 */
public class VeliPaperSettingsActivity extends FragmentActivity {

    private PreviewFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragment = (PreviewFragment) getSupportFragmentManager().findFragmentById(R.id.preview_fragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        mFragment.setFromSettingsScreen(TextUtils.isEmpty(intent.getAction()));

    }
}
