package com.velidev.velipaper.di.module;

import android.content.Context;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.VeliApplication;
import com.velidev.velipaper.drawable.AnalogClockDrawable;
import com.velidev.velipaper.drawable.BitmapBackgroundDrawable;
import com.velidev.velipaper.drawable.CircularDateDrawable;
import com.velidev.velipaper.drawable.ColorBackgroundDrawable;
import com.velidev.velipaper.drawable.RssBackgroundDrawable;
import com.velidev.velipaper.drawable.SaturnClockDrawable;
import com.velidev.velipaper.drawable.TransitionDrawable;
import com.velidev.velipaper.log.Logger;
import com.velidev.velipaper.service.VeliPaperService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nikola on 3.2.2015..
 */

@Module(
        library = true,
        injects = {
                TransitionDrawable.class, VeliApplication.class,AnalogClockDrawable.class, ColorBackgroundDrawable.class, VeliPaperService.class, BitmapBackgroundDrawable.class, CircularDateDrawable.class, SaturnClockDrawable.class, RssBackgroundDrawable.class
        }
        ,
        includes = {
                LogProviderModule.class,
        }
)
public class ColorProviderModule {


    private Context mContext;

    public ColorProviderModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    public Config.ColorProvider provideColorProvider() {
        return new Config.ColorProvider(mContext);
    }

}
