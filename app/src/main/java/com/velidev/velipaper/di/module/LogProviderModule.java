package com.velidev.velipaper.di.module;

import com.velidev.velipaper.activity.VeliPaperSettingsActivity;
import com.velidev.velipaper.di.annotation.LogTagClass;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.drawable.VeliDrawable;
import com.velidev.velipaper.log.Logger;
import com.velidev.velipaper.service.VeliPaperService;
import com.velidev.velipaper.ui.fragment.ImagePickerFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nikola on 2/13/2015.
 */
@Module(
        library = true
)
public class LogProviderModule {

    @Provides
    Logger provideDefaultLogger() {
        return Logger.get();
    }

    @Provides
    @Named("VeliPaperService")
    Logger provideServiceLogger() {
        return Logger.get(VeliPaperService.class);
    }

    @Provides
    @LogTagClass(VeliPaperSettingsActivity.class)
    Logger provideStartActivityLogger() {
        return Logger.get(VeliPaperSettingsActivity.class);
    }

    @Provides
    @Named("VeliDrawable")
    Logger provideVeliDrawableLogger() {
        return Logger.get(VeliDrawable.class);
    }

    @Provides
    @Named("ImagePickerFragment")
    Logger provideImagePickerLogger() {
        return Logger.get(ImagePickerFragment.class);
    }


}
