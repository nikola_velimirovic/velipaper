package com.velidev.velipaper.di.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Nikola on 2/13/2015.
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface Named {

    String value() default "";
}