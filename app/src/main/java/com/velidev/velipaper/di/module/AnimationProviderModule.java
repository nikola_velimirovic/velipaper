package com.velidev.velipaper.di.module;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.velidev.velipaper.Config;
import com.velidev.velipaper.R;
import com.velidev.velipaper.di.annotation.Named;
import com.velidev.velipaper.ui.fragment.ColorPickerFragment;
import com.velidev.velipaper.ui.fragment.PreviewFragment;
import com.velidev.velipaper.ui.view.VeliCheckBox;
import com.velidev.velipaper.ui.view.VeliSeekBar;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nikola on 2/14/2015.
 */

@Module(
        library = true,
        injects = {
        PreviewFragment.class, VeliSeekBar.class, VeliCheckBox.class, ColorPickerFragment.class
}
)
public class AnimationProviderModule {

    private Context mContext;

    public AnimationProviderModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    @Named("pushTopIn")
    public Animation providePushTopInAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_top_in);
        animation.setDuration(Config.ANIMATION_TIME);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("pushTopOut")
    public Animation providePushTopOutAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_top_out);
        animation.setDuration(Config.ANIMATION_TIME);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("scaleIn")
    public Animation provideScaleInAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.scale_in);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("scaleOut")
    public Animation provideScaleOutAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.scale_out);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("fadeIn")
    public Animation provideFadeInAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("fadeOut")
    public Animation provideFadeOutAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("pushBottomIn")
    public Animation providePushBottomInAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_bottom_in);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }

    @Provides
    @Singleton
    @Named("pushBottomOut")
    public Animation providePushBottomOutAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_bottom_out);
        animation.setDuration(Config.ANIMATION_TIME_SHORTEST);
        animation.setFillAfter(true);
        return animation;
    }


}
